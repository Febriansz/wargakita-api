CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -ldflags="-w -s" -o wargakita ./cmd/

docker build -t wargakita .
docker tag wargakita:latest 156256161559.dkr.ecr.ap-southeast-1.amazonaws.com/wargakita:latest
# shellcheck disable=SC2091
aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 156256161559.dkr.ecr.ap-southeast-1.amazonaws.com

docker push 156256161559.dkr.ecr.ap-southeast-1.amazonaws.com/wargakita:latest
package contracts

import (
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
)

type AkunRepository interface {
	FindById(id int) (akun entities.Akun, err error)
	FindByPassword(email, password string) (akun entities.Akun, err error)
	Insert(register requests.Register) (err error)
}

type AkunService interface {
	GetById(id int) (akun entities.Akun, err error)
	GetByPassword(email, password string) (akun entities.Akun, err error)
	Register(register requests.Register) (err error)
	RefreshToken(jwtIssuer, jwtKey string, jwtLifetime int, akun entities.Akun) (token string, err error)
}

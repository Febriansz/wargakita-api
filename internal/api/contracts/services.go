package contracts

type Services struct {
	Akun    AkunService
	Iuran   IuranService
	Laporan LaporanService
	Logging LoggingService
	Rumah   RumahService
	Warga   WargaService
}

package contracts

import (
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
)

type RumahRepository interface {
	FindAll() (daftarRumah []entities.Rumah)
	FindById(id int) (rumah entities.Rumah, err error)
	FindPenghuni(idRumah int) (daftarPenghuni []entities.Penghuni)
	Insert(rumah requests.TambahRumah) (err error)
	Delete(idRumah int) (err error)
	InsertPenghuni(penghuni requests.ParamsPenghuni) (err error)
	DeletePenghuni(penghuni requests.ParamsPenghuni) (err error)
}

type RumahService interface {
	GetAll() (daftarRumah []entities.Rumah)
	GetById(id int) (rumah entities.Rumah, err error)
	GetPenghuni(idRumah int) (daftarPenghuni []entities.Penghuni)
	Add(rumah requests.TambahRumah) (err error)
	Remove(idRumah int) (err error)
	AddPenghuni(penghuni requests.ParamsPenghuni) (err error)
	RemovePenghuni(penghuni requests.ParamsPenghuni) (err error)
}

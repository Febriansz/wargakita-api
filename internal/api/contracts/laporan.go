package contracts

import (
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/responses"
)

type LaporanRepository interface {
	FindAllOpen() (daftarLaporan []responses.Laporan)
	FindNewest() (daftarLaporan []responses.Laporan)
	FindById(id int) (laporan entities.Laporan, err error)
	CloseReport(laporan entities.TutupLaporan) (err error)
}

type LaporanService interface {
	GetAllOpen() (daftarLaporan []responses.Laporan)
	GetNewest() (daftarLaporan []responses.Laporan)
	GetById(id int) (laporan entities.Laporan, err error)
	CloseReport(laporan entities.TutupLaporan) (err error)
}

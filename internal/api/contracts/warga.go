package contracts

import (
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
)

type WargaRepository interface {
	CountTotal() (total int)
	FindAll() (daftarWarga []responses.Warga)
	FindNonResidentOnly() (daftarWarga []responses.Warga)
	FindById(id int) (warga responses.Warga, err error)
	Insert(warga requests.TambahWarga) (err error)
}

type WargaService interface {
	GetTotal() (total int)
	GetAll() (daftarWarga []responses.Warga)
	GetNonResidentOnly() (daftarWarga []responses.Warga)
	GetById(id int) (warga responses.Warga, err error)
	Add(warga requests.TambahWarga) (err error)
}

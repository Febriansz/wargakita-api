package contracts

import (
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
)

type IuranRepository interface {
	SumTotal() (total int)
	FindAll() (daftarIuran []responses.Iuran)
	FindById(id int) (iuran responses.Iuran, err error)
	FindByKtp(ktp string) (iuran responses.Iuran, err error)
	Insert(iuran requests.TambahIuran) (err error)
}

type IuranService interface {
	GetTotal() (total int)
	GetAll() (daftarIuran []responses.Iuran)
	GetById(id int) (iuran responses.Iuran, err error)
	GetByKtp(ktp string) (iuran responses.Iuran, err error)
	Add(iuran requests.TambahIuran) (err error)
}

package contracts

import (
	"github.com/kataras/iris/v12"
)

type App struct {
	Datasources *Datasources
	Iris        *iris.Application
	Services    *Services
}

package entities

type Laporan struct {
	Id        int    `db:"id" json:"id"`
	IdWarga   int    `db:"id_warga" json:"id_warga"`
	NoKtp     string `db:"no_ktp" json:"no_ktp"`
	Nama      string `db:"nama_warga" json:"nama_warga"`
	Ringkasan string `db:"ringkasan" json:"ringkasan"`
	Deskripsi string `db:"deskripsi" json:"deskripsi"`
	IdAmin    int    `db:"id_admin" json:"id_admin"`
	Respon    string `db:"respon" json:"respon"`
}

type TutupLaporan struct {
	Id     int `db:"id" json:"id"`
	IdAmin int `db:"id_admin" json:"id_admin"`
	Respon string `db:"respon" json:"respon"`
}

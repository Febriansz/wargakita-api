package entities

type Akun struct {
	Id           int    `db:"id" json:"id"`
	Nama         string `db:"nama" json:"nama"`
	Email        string `db:"email" json:"email"`
	Password     string `db:"password" json:"-"`
	NomorTelepon string `db:"nomor_telepon" json:"telepon"`
	JwtToken     string `db:"-" json:"jwt_token"`
}
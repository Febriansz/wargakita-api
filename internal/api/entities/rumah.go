package entities

type Rumah struct {
	Id             string `db:"id" json:"id"`
	Alamat         string `db:"alamat" json:"alamat"`
	Status         string `db:"status" json:"status"`
	JumlahPenghuni string `db:"jumlah_penghuni" json:"jumlah_penghuni"`
}

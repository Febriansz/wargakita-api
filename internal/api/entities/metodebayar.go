package entities

type MetodeBayar struct {
	Id        int    `db:"id" json:"id"`
	Metode    string `db:"metode" json:"metode"`
}

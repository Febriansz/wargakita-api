package datasources

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/pkg/alog"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"os"
)

// Prepare prepare sql statements or exit api if fails or error
func Prepare(db *sqlx.DB, query string) *sqlx.Stmt {
	s, err := db.Preparex(query)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("error while preparing statement: %s", err)))
		alog.Error(errors.New(fmt.Sprintf("query: %s", query)))

		os.Exit(constants.ExitPrepareStmtFail)
	}
	return s
}

// PrepareNamed prepare sql statements with named bindvars or exit api if fails or error
func PrepareNamed(db *sqlx.DB, query string) *sqlx.NamedStmt {
	s, err := db.PrepareNamed(query)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("error while preparing statement: %s", err)))
		alog.Error(errors.New(fmt.Sprintf("query: %s", query)))

		os.Exit(constants.ExitPrepareStmtFail)
	}
	return s
}

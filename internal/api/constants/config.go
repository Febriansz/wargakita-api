package constants

const (
	ServerPort   = "server.port"
)

const (
	JwtIssuer   = "jwt.issuer"
	JwtKey      = "jwt.key"
	JwtLifetime = "jwt.lifetime"
)

const (
	DbHostWriter = "db.host.writer"
	DbHostReader = "db.host.reader"
	DbPort       = "db.port"
	DbName       = "db.name"
	DbUser       = "db.user"
	DbPass       = "db.pass"
)

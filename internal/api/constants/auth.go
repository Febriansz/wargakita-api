package constants

const (
	ValueClaim   = "VALUE_CLAIM"
	ValueId      = "VALUE_ID"
	ValueNama    = "VALUE_NAMA"
	ValueEmail   = "VALUE_EMAIL"
	ValueTelepon = "VALUE_TELEPON"
)

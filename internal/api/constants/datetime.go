package constants

const (
	DateTimeFormatUsWithZone = "2006-01-02 15:04:05 +0700"
	DateTimeFormatUs         = "2006-01-02 15:04:05"
	DateFormatUs             = "2006-01-02"
	DateFormatShort          = "02 Jan"
	DateTimeFormatDefault    = "15:04, 02 Jan 2006"
	DateMonthFormatDefault   = "Jan 2006"
	TimeDefaultFormat        = "15:04:05"
)

package routers

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/handlers"
	"api.wargakita.id/internal/api/middlewares"
	"github.com/kataras/iris/v12"
)

func Init(app *contracts.App, crs iris.Handler) {

	app.Iris.Get("/", func(context iris.Context) {

	})

	party := app.Iris.Party("/v1", crs)
	{
		party.Post("/login", handlers.Login)
		party.Post("/register", handlers.Register)

		party.Get("/dashboard", middlewares.Auth, handlers.Dashboard)

		party.Get("/warga", middlewares.Auth, handlers.GetAllWarga)
		party.Post("/warga", middlewares.Auth, handlers.AddWarga)

		party.Get("/iuran", middlewares.Auth, handlers.GetAllIuran)
		party.Post("/iuran", middlewares.Auth, handlers.AddIuran)

		party.Get("/rumah", middlewares.Auth, handlers.GetAllRumah)
		party.Post("/rumah", middlewares.Auth, handlers.AddRumah)

		party.Get("/penghuni/{id_rumah:int}", middlewares.Auth, handlers.GetPenghuni)
		party.Post("/penghuni", middlewares.Auth, handlers.AddPenghuni)
		party.Delete("/penghuni", middlewares.Auth, handlers.DeletePenghuni)

		party.Get("/laporan", middlewares.Auth, handlers.GetAllLaporan)
	}
}

package handlers

import (
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/ahttp"
	"github.com/kataras/iris/v12"
)

func GetAllIuran(c iris.Context) {
	daftarIuran := app.Services.Iuran.GetAll()

	HttpSuccess(c, daftarIuran, "")
}

func AddIuran(c iris.Context) {
	params := requests.TambahIuran{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Iuran.Add(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}
package handlers

import (
	"api.wargakita.id/pkg/ahttp"
	"api.wargakita.id/pkg/alog"
	"github.com/kataras/iris/v12"
)

func HttpError(c iris.Context, httpError ahttp.ErrorResponse, err error) {
	c.ResponseWriter().Header().Del("Access-Control-Allow-Origin")
	c.ResponseWriter().Header().Del("Access-Control-Allow-Credentials")
	c.ResponseWriter().Header().Set("Access-Control-Allow-Origin", "*")
	c.ResponseWriter().Header().Set("Access-Control-Allow-Credentials", "true")
	c.StatusCode(httpError.Status)

	alog.Error(err)

	//if os.Getenv(constants.ServerEnv) == constants.EnvDevelopment {
		traces := alog.GetTracer(err)

		response := ahttp.ErrorDebugResponse{
			Status:  httpError.Status,
			Code:    httpError.Code,
			Message: httpError.Message,
			Debug:   traces,
		}

		c.JSON(response)
	//} else {
	//	response := ahttp.ErrorResponse{
	//		Status:  httpError.Status,
	//		Code:    httpError.Code,
	//		Message: err.Error(),
	//	}
	//
	//	c.JSON(response)
	//}

	c.StopExecution()
	return
}

func HttpSuccess(c iris.Context, data interface{}, message string) {
	c.ResponseWriter().Header().Del("Access-Control-Allow-Origin")
	c.ResponseWriter().Header().Del("Access-Control-Allow-Credentials")
	c.ResponseWriter().Header().Set("Access-Control-Allow-Origin", "*")
	c.ResponseWriter().Header().Set("Access-Control-Allow-Credentials", "true")

	response := ahttp.Response{}
	response.Status = ahttp.ResponseSuccess
	response.Message = message

	if data != nil {
		response.Data = data
	}

	c.JSON(response)
	c.StopExecution()
	return
}

package handlers

import (
	"github.com/kataras/iris/v12"
)

func GetAllLaporan(c iris.Context) {
	obj := app.Services.Laporan.GetAllOpen()

	HttpSuccess(c, obj, "")
}
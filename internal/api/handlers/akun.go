package handlers

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/ahttp"
	"github.com/kataras/iris/v12"
	"github.com/spf13/viper"
)

func Register(c iris.Context) {
	params := requests.Register{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Akun.Register(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}

func Login(c iris.Context) {
	params := requests.Login{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	akun, err := app.Services.Akun.GetByPassword(params.Email, params.Password)
	if err != nil {
		HttpError(c, ahttp.ErrUnauthorized, err)
		return
	}

	jwtIssuer := viper.GetString(constants.JwtIssuer)
	jwtKey := viper.GetString(constants.JwtKey)
	jwtLifetime := viper.GetInt(constants.JwtLifetime)

	akun.JwtToken, err = app.Services.Akun.RefreshToken(jwtIssuer, jwtKey, jwtLifetime, akun)
	if err != nil {
		HttpError(c, ahttp.ErrUnauthorized, err)
		return
	}

	HttpSuccess(c, akun, "")
}
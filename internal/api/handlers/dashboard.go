package handlers

import (
	"api.wargakita.id/internal/api/models/responses"
	"github.com/kataras/iris/v12"
)

func Dashboard(c iris.Context) {
	obj := responses.Dashboard{}
	obj.TotalWarga = app.Services.Warga.GetTotal()
	obj.TotalIuran = app.Services.Iuran.GetTotal()
	obj.DaftarLaporan = app.Services.Laporan.GetNewest()

	HttpSuccess(c, obj, "")
}
package handlers

import (
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/ahttp"
	"github.com/kataras/iris/v12"
)

func GetAllRumah(c iris.Context) {
	daftarRumah := app.Services.Rumah.GetAll()

	HttpSuccess(c, daftarRumah, "")
}

func AddRumah(c iris.Context) {
	params := requests.TambahRumah{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Rumah.Add(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}

func GetPenghuni(c iris.Context) {
	idRumah, _ := c.Params().GetInt("id_rumah")

	daftarPenghuni := app.Services.Rumah.GetPenghuni(idRumah)
	if daftarPenghuni == nil {
		daftarPenghuni = []entities.Penghuni{}
	}

	HttpSuccess(c, daftarPenghuni, "")
}

func AddPenghuni(c iris.Context) {
	params := requests.ParamsPenghuni{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Rumah.AddPenghuni(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}

func DeletePenghuni(c iris.Context) {
	params := requests.ParamsPenghuni{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Rumah.RemovePenghuni(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}

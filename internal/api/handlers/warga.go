package handlers

import (
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
	"api.wargakita.id/pkg/ahttp"
	"github.com/kataras/iris/v12"
)

func GetAllWarga(c iris.Context) {
	isNonResidentOnly := false

	isNonResidentOnly, _ = c.URLParamBool("is_non_resident_only")

	daftarWarga := []responses.Warga{}
	if isNonResidentOnly {
		daftarWarga = app.Services.Warga.GetNonResidentOnly()
	} else {
		daftarWarga = app.Services.Warga.GetAll()
	}

	HttpSuccess(c, daftarWarga, "")
}

func AddWarga(c iris.Context) {
	params := requests.TambahWarga{}
	err := c.ReadJSON(&params)
	if err != nil {
		HttpError(c, ahttp.ErrBadRequest, err)
		return
	}

	err = app.Services.Warga.Add(params)
	if err != nil {
		HttpError(c, ahttp.ErrInternalServer, err)
		return
	}

	HttpSuccess(c, nil, "")
}
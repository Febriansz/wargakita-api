package akun

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/datasources"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/alog"
	"api.wargakita.id/pkg/asql"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
	findById       *sqlx.Stmt
	findByPassword *sqlx.Stmt
	insert         *sqlx.NamedStmt
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.AkunRepository {
	stmts := Statement{
		findById:       datasources.Prepare(dbReader, findById),
		findByPassword: datasources.Prepare(dbReader, findByPassword),
		insert:         datasources.PrepareNamed(dbWriter, insert),
	}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

func (r Repository) FindById(id int) (akun entities.Akun, err error) {
	row := r.stmt.findById.QueryRow(id)
	err = row.Scan(
		&akun.Id,
		&akun.Nama,
		&akun.Email,
		&akun.Password,
		&akun.NomorTelepon,
	)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get akun by id %v, err: %v", id, err)))
	}
	return
}

func (r Repository) FindByPassword(email, password string) (akun entities.Akun, err error) {
	md5Password := md5.Sum([]byte(password))
	password = hex.EncodeToString(md5Password[:])

	row := r.stmt.findByPassword.QueryRow(email, password)
	err = row.Scan(
		&akun.Id,
		&akun.Nama,
		&akun.Email,
		&akun.Password,
		&akun.NomorTelepon,
	)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get akun by password for email %v, err: %v", email, err)))
	}

	return
}

func (r Repository) Insert(register requests.Register) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	md5Password := md5.Sum([]byte(register.Password))
	register.Password = hex.EncodeToString(md5Password[:])

	_, err = tx.NamedStmt(r.stmt.insert).Exec(register)
	return err
}

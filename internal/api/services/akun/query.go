package akun

const (
	findById = `
		SELECT 
			id, nama, email, password, nomor_telepon 
		FROM akun 
		WHERE 
			id = ?
	`

	findByPassword = `
		SELECT 
			id, nama, email, password, nomor_telepon 
		FROM akun 
		WHERE 
			email = ? AND 
			password = ?
	`

	insert = `
		INSERT INTO akun ( 
			nama,
			email,
			nomor_telepon,
			password
		) VALUES (
			:nama,
			:email,
			:telepon,
			:password
		)
	`
)

package akun

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type Service struct {
	repo contracts.AkunRepository
}

func Init(app *contracts.App) (svc contracts.AkunService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)

	svc = &Service{
		repo: r,
	}

	return
}

func (s Service) GetById(id int) (akun entities.Akun, err error) {
	akun, err = s.repo.FindById(id)
	return
}

func (s Service) GetByPassword(email, password string) (akun entities.Akun, err error) {
	akun, err = s.repo.FindByPassword(email, password)
	return
}

func (s Service) Register(register requests.Register) (err error) {
	err = s.repo.Insert(register)
	return
}

func (s *Service) RefreshToken(jwtIssuer, jwtKey string, jwtLifetime int, akun entities.Akun) (token string, err error) {
	claims := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		requests.Claims{
			StandardClaims: jwt.StandardClaims{
				Issuer:    jwtIssuer,
				ExpiresAt: time.Now().Add(time.Duration(jwtLifetime) * time.Second).Unix(),
			},
			Id:           akun.Id,
			Nama:         akun.Nama,
			Email:        akun.Email,
			NomorTelepon: akun.NomorTelepon,
		},
	)

	return claims.SignedString([]byte(jwtKey))
}

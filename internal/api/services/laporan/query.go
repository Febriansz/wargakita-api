package laporan

const (
	findAllOpen = `
		SELECT 
			l.id, w.nama AS nama_warga, l.ringkasan, l.created_at  
		FROM laporan l
			INNER JOIN warga w ON l.id_warga = w.id
		WHERE 
			l.id_admin IS NULL
		ORDER BY 
			created_at DESC
	`

	findNewest = `
		SELECT 
			l.id, w.nama AS nama_warga, l.ringkasan, l.created_at  
		FROM laporan l
			INNER JOIN warga w ON l.id_warga = w.id
		WHERE 
			l.id_admin IS NULL
		ORDER BY 
			created_at DESC
		LIMIT 10
	`

	findById = `
		SELECT 
			l.id, l.id_warga, w.no_ktp, w.nama AS nama_warga, l.ringkasan, l.deskripsi, 
			l.id_admin, a.nama AS nama_admin, l.respon  
		FROM laporan l
			INNER JOIN warga w ON l.id_warga = w.id
			INNER JOIN akun a ON l.id_admin = a.id
		WHERE 
			l.id = ?
	`

	closeReport = `
		UPDATE laporan
		SET
			id_admin = :id_admin,
			respon = :respon
		WHERE 
			id = :id
	`
)

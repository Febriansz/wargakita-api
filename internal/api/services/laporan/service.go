package laporan

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/responses"
	"time"
)

type Service struct {
	app  *contracts.App
	repo contracts.LaporanRepository
}

func Init(app *contracts.App) (svc contracts.LaporanService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)

	svc = &Service{
		app:  app,
		repo: r,
	}

	return
}

func (s Service) GetAllOpen() (daftarLaporan []responses.Laporan) {
	daftarLaporan = s.repo.FindAllOpen()

	for k, v := range daftarLaporan {
		createdAt, err := time.Parse(constants.DateTimeFormatUs, v.CreatedAt)
		if err == nil {
			daftarLaporan[k].Tanggal = createdAt.Format(constants.DateTimeFormatDefault)
		}
	}

	return
}

func (s Service) GetNewest() (daftarLaporan []responses.Laporan) {
	daftarLaporan = s.repo.FindNewest()

	for k, v := range daftarLaporan {
		createdAt, err := time.Parse(constants.DateTimeFormatUs, v.CreatedAt)
		if err == nil {
			daftarLaporan[k].Tanggal = createdAt.Format(constants.DateTimeFormatDefault)
		}
	}

	return
}

func (s Service) GetById(id int) (laporan entities.Laporan, err error) {
	laporan, err = s.repo.FindById(id)
	return
}

func (s Service) CloseReport(laporan entities.TutupLaporan) (err error) {
	err = s.repo.CloseReport(laporan)
	return
}

package laporan

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/datasources"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/responses"
	"api.wargakita.id/pkg/alog"
	"api.wargakita.id/pkg/asql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
	findAllOpen *sqlx.Stmt
	findNewest  *sqlx.Stmt
	findById    *sqlx.Stmt
	closeReport *sqlx.NamedStmt
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.LaporanRepository {

	stmts := Statement{
		findAllOpen: datasources.Prepare(dbReader, findAllOpen),
		findNewest:  datasources.Prepare(dbReader, findNewest),
		findById:    datasources.Prepare(dbReader, findById),
		closeReport: datasources.PrepareNamed(dbWriter, closeReport),
	}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

func (r Repository) FindAllOpen() (daftarLaporan []responses.Laporan) {
	err := r.stmt.findAllOpen.Select(&daftarLaporan)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get all open laporan, err: %v", err)))
	}
	return
}

func (r Repository) FindNewest() (daftarLaporan []responses.Laporan) {
	err := r.stmt.findNewest.Select(&daftarLaporan)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get newest laporan, err: %v", err)))
	}
	return
}

func (r Repository) FindById(id int) (laporan entities.Laporan, err error) {
	row := r.stmt.findById.QueryRow(id)
	err = row.Scan(&laporan)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get iuran by id %v, err: %v", id, err)))
	}
	return
}

func (r Repository) CloseReport(laporan entities.TutupLaporan) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.closeReport).Exec(laporan)
	return err
}

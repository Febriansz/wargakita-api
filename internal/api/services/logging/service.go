package logging

import "api.wargakita.id/internal/api/contracts"

type Service struct {
	repo contracts.LoggingRepository
}

func Init(app *contracts.App) (svc contracts.LoggingService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)
	svc = &Service{repo: r}

	return
}

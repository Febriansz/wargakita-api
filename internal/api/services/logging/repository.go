package logging

import (
	"api.wargakita.id/internal/api/contracts"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.LoggingRepository {
	stmts := Statement{}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

package rumah

const (
	findAll = `
		SELECT 
			id, CONCAT(alamat, ' RT ', rt, ' RW ', rw) AS alamat, status,
			(SELECT COUNT(id_warga) FROM rumah_warga WHERE id_rumah = rumah.id) AS jumlah_penghuni 
		FROM rumah 
		ORDER BY rw, rt
	`

	findById = `
		SELECT 
			id, CONCAT(alamat, ' RT ', rt, ' RW ', rw) AS alamat, status,
			(SELECT COUNT(id_warga) FROM rumah_warga WHERE id_rumah = rumah.id) AS jumlah_penghuni 
		FROM rumah 
			WHERE id = ?
	`

	findPenghuni = `SELECT w.id, w.nama AS nama_warga FROM rumah_warga rw INNER JOIN warga w ON rw.id_warga = w.id WHERE id_rumah = ?`

	insert = `
		INSERT INTO rumah ( 
			rt,
			rw,
			alamat
		) VALUES (
			:rt,
			:rw,
			:alamat
		)
	`

	deleteRumah = `DELETE FROM rumah WHERE id = ?`

	insertPenghuni = `
		INSERT INTO rumah_warga ( 
			id_warga,
			id_rumah
		) VALUES (
			:id_warga,
			:id_rumah
		)
	`

	deletePenghuni = `DELETE FROM rumah_warga WHERE id_warga = :id_warga AND id_rumah = :id_rumah`
)

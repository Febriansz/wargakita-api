package rumah

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
)

type Service struct {
	app  *contracts.App
	repo contracts.RumahRepository
}

func Init(app *contracts.App) (svc contracts.RumahService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)

	svc = &Service{
		app:  app,
		repo: r,
	}

	return
}

func (s Service) GetAll() (daftarRumah []entities.Rumah) {
	daftarRumah = s.repo.FindAll()
	return
}

func (s Service) GetPenghuni(idRumah int) (daftarPenghuni []entities.Penghuni) {
	daftarPenghuni = s.repo.FindPenghuni(idRumah)
	return
}

func (s Service) GetById(id int) (rumah entities.Rumah, err error) {
	rumah, err = s.repo.FindById(id)
	return
}

func (s Service) Add(rumah requests.TambahRumah) (err error) {
	err = s.repo.Insert(rumah)
	return
}

func (s Service) Remove(idRumah int) (err error) {
	err = s.repo.Delete(idRumah)
	return
}

func (s Service) AddPenghuni(penghuni requests.ParamsPenghuni) (err error) {
	err = s.repo.InsertPenghuni(penghuni)
	return
}

func (s Service) RemovePenghuni(penghuni requests.ParamsPenghuni) (err error) {
	err = s.repo.DeletePenghuni(penghuni)
	return
}

package rumah

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/datasources"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/alog"
	"api.wargakita.id/pkg/asql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
	findAll      *sqlx.Stmt
	findById     *sqlx.Stmt
	findPenghuni *sqlx.Stmt
	insert       *sqlx.NamedStmt
	delete       *sqlx.Stmt
	insertWarga  *sqlx.NamedStmt
	deleteWarga  *sqlx.NamedStmt
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.RumahRepository {

	stmts := Statement{
		findAll:      datasources.Prepare(dbReader, findAll),
		findById:     datasources.Prepare(dbReader, findById),
		findPenghuni: datasources.Prepare(dbReader, findPenghuni),
		insert:       datasources.PrepareNamed(dbWriter, insert),
		delete:       datasources.Prepare(dbWriter, deleteRumah),
		insertWarga:  datasources.PrepareNamed(dbWriter, insertPenghuni),
		deleteWarga:  datasources.PrepareNamed(dbWriter, deletePenghuni),
	}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

func (r Repository) FindAll() (daftarRumah []entities.Rumah) {
	err := r.stmt.findAll.Select(&daftarRumah)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get all rumah, err: %v", err)))
	}
	return
}

func (r Repository) FindPenghuni(idRumah int) (daftarPenghuni []entities.Penghuni) {
	err := r.stmt.findPenghuni.Select(&daftarPenghuni, idRumah)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get all rumah, err: %v", err)))
	}
	return
}

func (r Repository) FindById(id int) (rumah entities.Rumah, err error) {
	row := r.stmt.findById.QueryRow(id)
	err = row.Scan(
		&rumah.Id,
		&rumah.Alamat,
		&rumah.Status,
		&rumah.JumlahPenghuni,
	)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get rumah by id %v, err: %v", id, err)))
	}
	return
}

func (r Repository) Insert(rumah requests.TambahRumah) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.insert).Exec(rumah)
	return err
}

func (r Repository) Delete(idRumah int) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.Stmtx(r.stmt.delete).Exec(idRumah)
	return err
}

func (r Repository) InsertPenghuni(penghuni requests.ParamsPenghuni) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.insertWarga).Exec(penghuni)
	return err
}

func (r Repository) DeletePenghuni(penghuni requests.ParamsPenghuni) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.deleteWarga).Exec(penghuni)
	return err
}

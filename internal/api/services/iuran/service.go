package iuran

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
	"time"
)

type Service struct {
	app  *contracts.App
	repo contracts.IuranRepository
}

func Init(app *contracts.App) (svc contracts.IuranService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)

	svc = &Service{
		app:  app,
		repo: r,
	}

	return
}

func (s Service) GetTotal() (total int) {
	total = s.repo.SumTotal()
	return
}

func (s Service) GetAll() (daftarIuran []responses.Iuran) {
	daftarIuran = s.repo.FindAll()

	for k, v := range daftarIuran {
		createdAt, err := time.Parse(constants.DateTimeFormatUs, v.CreatedAt)
		if err == nil {
			daftarIuran[k].Tanggal = createdAt.Format(constants.DateMonthFormatDefault)
		}
	}

	return
}

func (s Service) GetById(id int) (iuran responses.Iuran, err error) {
	iuran, err = s.repo.FindById(id)
	return
}

func (s Service) GetByKtp(ktp string) (iuran responses.Iuran, err error) {
	iuran, err = s.repo.FindByKtp(ktp)
	return
}

func (s Service) Add(iuran requests.TambahIuran) (err error) {
	err = s.repo.Insert(iuran)
	return
}

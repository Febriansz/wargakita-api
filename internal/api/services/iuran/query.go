package iuran

const (
	sumTotal = `SELECT sum(jumlah) FROM iuran`

	findAll = `
		SELECT 
			i.id, w.nama AS nama_warga, j.nama AS jenis_iuran, 
			i.jumlah, i.created_at  
		FROM iuran i
			INNER JOIN warga w ON i.id_warga = w.id
			INNER JOIN jenis_iuran j ON i.id_jenis_iuran = j.id
		ORDER BY
			i.created_at DESC
	`

	findById = `
		SELECT 
			i.id, i.id_warga, w.no_ktp, w.nama AS nama_warga, i.id_jenis_iuran, j.nama AS jenis_iuran, 
			i.bulan, i.tahun, i.id_metode_bayar, m.metode AS metode_bayar, i.jumlah  
		FROM iuran i
			INNER JOIN warga w ON i.id_warga = w.id
			INNER JOIN jenis_iuran j ON i.id_jenis_iuran = j.id
			INNER JOIN metode_bayar m ON i.id_metode_bayar = m.id
		WHERE 
			i.id = ?
	`

	findByKtp = `
		SELECT 
			i.id, i.id_warga, w.no_ktp, w.nama AS nama_warga, i.id_jenis_iuran, j.nama AS jenis_iuran, 
			i.bulan, i.tahun, i.id_metode_bayar, m.metode AS metode_bayar, i.jumlah 
		FROM iuran i
			INNER JOIN warga w ON i.id_warga = w.id
			INNER JOIN jenis_iuran j ON i.id_jenis_iuran = j.id
			INNER JOIN metode_bayar m ON i.id_metode_bayar = m.id
		WHERE 
			w.no_ktp = ?
	`

	insert = `
		INSERT INTO iuran ( 
			id_warga,
			id_jenis_iuran,
			bulan,
			tahun,
			id_metode_bayar,
			jumlah
		) VALUES (
			:id_warga,
			:id_jenis_iuran,
			MONTH(CURDATE()),
			YEAR(CURDATE()),
			:id_metode_bayar,
			:jumlah
		)
	`
)

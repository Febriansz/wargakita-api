package iuran

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/datasources"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
	"api.wargakita.id/pkg/alog"
	"api.wargakita.id/pkg/asql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
	sumTotal *sqlx.Stmt
	findAll  *sqlx.Stmt
	findById *sqlx.Stmt
	insert   *sqlx.NamedStmt
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.IuranRepository {

	stmts := Statement{
		sumTotal: datasources.Prepare(dbReader, sumTotal),
		findAll:  datasources.Prepare(dbReader, findAll),
		findById: datasources.Prepare(dbReader, findById),
		insert:   datasources.PrepareNamed(dbWriter, insert),
	}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

func (r Repository) SumTotal() (total int) {
	row := r.stmt.sumTotal.QueryRow()
	err := row.Scan(&total)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get total iuran, err: %v", err)))
	}
	return
}

func (r Repository) FindAll() (daftarIuran []responses.Iuran) {
	err := r.stmt.findAll.Select(&daftarIuran)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get all iuran, err: %v", err)))
	}
	return
}

func (r Repository) FindById(id int) (iuran responses.Iuran, err error) {
	row := r.stmt.findById.QueryRow(id)
	err = row.Scan(&iuran)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get iuran by id %v, err: %v", id, err)))
	}
	return
}

func (r Repository) FindByKtp(ktp string) (iuran responses.Iuran, err error) {
	row := r.stmt.findById.QueryRow(ktp)
	err = row.Scan(&iuran)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get iuran by ktp %v, err: %v", ktp, err)))
	}
	return
}

func (r Repository) Insert(iuran requests.TambahIuran) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.insert).Exec(iuran)
	return err
}

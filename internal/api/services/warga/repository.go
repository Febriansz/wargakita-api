package warga

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/datasources"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
	"api.wargakita.id/pkg/alog"
	"api.wargakita.id/pkg/asql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type Repository struct {
	dbWriter *sqlx.DB
	dbReader *sqlx.DB
	stmt     Statement
}

type Statement struct {
	countTotal          *sqlx.Stmt
	findAll             *sqlx.Stmt
	findNonResidentOnly *sqlx.Stmt
	findById            *sqlx.Stmt
	insert              *sqlx.NamedStmt
}

func initRepository(dbWriter *sqlx.DB, dbReader *sqlx.DB) contracts.WargaRepository {

	stmts := Statement{
		countTotal:          datasources.Prepare(dbReader, countTotal),
		findAll:             datasources.Prepare(dbReader, findAll),
		findNonResidentOnly: datasources.Prepare(dbReader, findNonResidentOnly),
		findById:            datasources.Prepare(dbReader, findById),
		insert:              datasources.PrepareNamed(dbWriter, insert),
	}

	r := Repository{
		dbWriter: dbWriter,
		dbReader: dbReader,
		stmt:     stmts,
	}

	return &r
}

func (r Repository) CountTotal() (total int) {
	row := r.stmt.countTotal.QueryRow()
	err := row.Scan(&total)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get total warga, err: %v", err)))
	}
	return
}

func (r Repository) FindAll() (daftarWarga []responses.Warga) {
	err := r.stmt.findAll.Select(&daftarWarga)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get all warga, err: %v", err)))
	}
	return
}

func (r Repository) FindNonResidentOnly() (daftarWarga []responses.Warga) {
	err := r.stmt.findNonResidentOnly.Select(&daftarWarga)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get non resident only warga, err: %v", err)))
	}
	return
}

func (r Repository) FindById(id int) (warga responses.Warga, err error) {
	row := r.stmt.findById.QueryRow(id)
	err = row.Scan(
		&warga.Id,
		&warga.NoKtp,
		&warga.Nama,
		&warga.JenisKelamin,
		&warga.NomorTelepon,
		&warga.Email,
	)
	if err != nil {
		alog.Error(errors.New(fmt.Sprintf("fail to get warga by id %v, err: %v", id, err)))
	}
	return
}

func (r Repository) Insert(warga requests.TambahWarga) (err error) {
	tx, err := r.dbWriter.Beginx()
	if err != nil {
		return err
	}
	defer asql.ReleaseTx(tx, &err)

	_, err = tx.NamedStmt(r.stmt.insert).Exec(warga)
	return err
}

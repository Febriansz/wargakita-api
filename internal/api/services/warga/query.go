package warga

const (
	countTotal = `SELECT COUNT(id) FROM warga`

	findAll = `
		SELECT 
			w.id, w.no_ktp, w.nama AS nama_warga, w.jenis_kelamin, w.nomor_telepon, w.email,
			IFNULL(r.alamat, '-') AS alamat_rumah, IFNULL(r.status, '-') AS status_rumah
		FROM warga w
			LEFT OUTER JOIN rumah_warga rw ON w.id = rw.id_warga
			LEFT OUTER JOIN rumah r ON rw.id_rumah = r.id
		ORDER BY
			w.nama
	`

	findNonResidentOnly = `
		SELECT 
			w.id, w.no_ktp, w.nama AS nama_warga, w.jenis_kelamin, w.nomor_telepon, w.email,
			IFNULL(r.alamat, '-') AS alamat_rumah, IFNULL(r.status, '-') AS status_rumah
		FROM warga w
			LEFT OUTER JOIN rumah_warga rw ON w.id = rw.id_warga
			LEFT OUTER JOIN rumah r ON rw.id_rumah = r.id
		WHERE
			rw.id IS NULL
		ORDER BY
			w.nama
	`

	findById = `
		SELECT 
			id, no_ktp, nama, jenis_kelamin, nomor_telepon, email 
		FROM warga 
		WHERE 
			id = ?
	`

	insert = `
		INSERT INTO warga ( 
			no_ktp,
			nama,
			jenis_kelamin,
			nomor_telepon,
			email
		) VALUES (
			:no_ktp,
			:nama,
			:jenis_kelamin,
			:nomor_telepon,
			:email
		)
	`
)

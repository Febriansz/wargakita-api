package warga

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/internal/api/models/responses"
)

type Service struct {
	app  *contracts.App
	repo contracts.WargaRepository
}

func Init(app *contracts.App) (svc contracts.WargaService) {
	r := initRepository(app.Datasources.WriterDB, app.Datasources.ReaderDB)

	svc = &Service{
		app:  app,
		repo: r,
	}

	return
}

func (s Service) GetTotal() (total int) {
	total = s.repo.CountTotal()
	return
}

func (s Service) GetAll() (daftarWarga []responses.Warga) {
	daftarWarga = s.repo.FindAll()
	return
}

func (s Service) GetNonResidentOnly() (daftarWarga []responses.Warga) {
	daftarWarga = s.repo.FindNonResidentOnly()
	return
}

func (s Service) GetById(id int) (warga responses.Warga, err error) {
	warga, err = s.repo.FindById(id)
	return
}

func (s Service) Add(warga requests.TambahWarga) (err error) {
	err = s.repo.Insert(warga)
	return
}

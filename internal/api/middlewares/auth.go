package middlewares

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/entities"
	"api.wargakita.id/internal/api/handlers"
	"api.wargakita.id/internal/pkg/ajwt"
	"api.wargakita.id/pkg/ahttp"
	"errors"
	"github.com/kataras/iris/v12"
	"github.com/spf13/viper"
)

func Auth(c iris.Context) {
	c.Header("Access-Control-Allow-Origin", "*")

	jwtToken := c.GetHeader("Authorization")
	if jwtToken == "" {
		handlers.HttpError(c, ahttp.ErrUnauthorized, errors.New("authorization is empty"))
		return
	}

	jwtIssuer := viper.GetString(constants.JwtIssuer)
	jwtKey := viper.GetString(constants.JwtKey)
	jwtLifetime := viper.GetInt(constants.JwtLifetime)

	claims, err := ajwt.ParseToken(jwtKey, jwtToken)
	if err != nil {
		handlers.HttpError(c, ahttp.ErrUnauthorized, err)
		c.Skip()
		return
	}

	c.Values().Set(constants.ValueClaim, claims)
	c.Values().Set(constants.ValueId, claims)
	c.Values().Set(constants.ValueNama, claims.Id)
	c.Values().Set(constants.ValueEmail, claims.Email)
	c.Values().Set(constants.ValueTelepon, claims.NomorTelepon)

	auth := entities.Akun{
		Id:           claims.Id,
		Nama:         claims.Nama,
		Email:        claims.Email,
		NomorTelepon: claims.NomorTelepon,
	}

	accessToken, err := app.Services.Akun.RefreshToken(jwtIssuer, jwtKey, jwtLifetime, auth)
	if err != nil {
		c.Skip()
		return
	}

	c.Header("Authorization", accessToken)
	c.Header("Access-Control-Expose-Headers", "Authorization")

	c.Next()
}

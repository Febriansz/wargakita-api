package responses

type Dashboard struct {
	TotalWarga    int       `db:"-" json:"total_warga"`
	TotalIuran    int       `db:"-" json:"total_iuran"`
	DaftarLaporan []Laporan `db:"-" json:"daftar_laporan"`
}

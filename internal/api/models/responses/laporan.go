package responses

type Laporan struct {
	Id        int    `db:"id" json:"-"`
	Nama      string `db:"nama_warga" json:"nama_warga"`
	Ringkasan string `db:"ringkasan" json:"ringkasan"`
	Respon    string `db:"respon" json:"-"`
	CreatedAt string `db:"created_at" json:"-"`
	Tanggal   string `db:"-" json:"tanggal"`
}

package responses

type Iuran struct {
	Id         int    `db:"id" json:"id"`
	Nama       string `db:"nama_warga" json:"nama_warga"`
	JenisIuran string `db:"jenis_iuran" json:"jenis_iuran"`
	Jumlah     int    `db:"jumlah" json:"jumlah"`
	CreatedAt  string `db:"created_at" json:"-"`
	Tanggal    string `db:"-" json:"tanggal"`
}

package responses

type Warga struct {
	Id           int    `db:"id" json:"id"`
	NoKtp        string `db:"no_ktp" json:"no_ktp"`
	Nama         string `db:"nama_warga" json:"nama_warga"`
	JenisKelamin string `db:"jenis_kelamin" json:"jenis_kelamin"`
	NomorTelepon string `db:"nomor_telepon" json:"nomor_telepon"`
	Email        string `db:"email" json:"email"`
	AlamatRumah  string `db:"alamat_rumah" json:"alamat_rumah"`
	StatusRumah  string `db:"status_rumah" json:"status_rumah"`
}

package requests

type TambahIuran struct {
	IdWarga       int `db:"id_warga" json:"id_warga"`
	IdJenisIuran  int `db:"id_jenis_iuran" json:"id_jenis_iuran"`
	IdMetodeBayar int `db:"id_metode_bayar" json:"id_metode_bayar"`
	Jumlah        int `db:"jumlah" json:"jumlah"`
}

package requests

type TambahRumah struct {
	RT     string `db:"rt" json:"rt"`
	RW     string `db:"rw" json:"rw"`
	Alamat string `db:"alamat" json:"alamat"`
	Status string `db:"status" json:"status"`
}

type ParamsPenghuni struct {
	IdRumah int `db:"id_rumah" json:"id_rumah"`
	IdWarga int `db:"id_warga" json:"id_warga"`
}

package requests

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	jwt.StandardClaims
	Id           int    `db:"id" json:"id"`
	Nama         string `db:"nama" json:"nama"`
	Email        string `db:"email" json:"email"`
	Password     string `db:"password" json:"password"`
	NomorTelepon string `db:"nomor_telepon" json:"nomor_telepon"`
	Level        string `db:"level" json:"level"`
}

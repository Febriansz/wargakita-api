package requests

type Register struct {
	Nama     string `db:"nama" json:"nama"`
	Email    string `db:"email" json:"email"`
	Telepon  string `db:"telepon" json:"telepon"`
	Password string `db:"password" json:"password"`
}

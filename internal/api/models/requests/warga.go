package requests

type TambahWarga struct {
	NoKtp        string `db:"no_ktp" json:"no_ktp"`
	Nama         string `db:"nama" json:"nama"`
	JenisKelamin string `db:"jenis_kelamin" json:"jenis_kelamin"`
	NomorTelepon string `db:"nomor_telepon" json:"nomor_telepon"`
	Email        string `db:"email" json:"email"`
}

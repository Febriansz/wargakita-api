package ajwt

import (
	"api.wargakita.id/internal/api/models/requests"
	"api.wargakita.id/pkg/ahttp"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
)

func ParseToken(jwtKey, jwtToken string) (*requests.Claims, error) {
	claims := &requests.Claims{}

	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, ahttp.ErrUnauthorized
		} else if method != jwt.SigningMethodHS256 {
			return nil, ahttp.ErrUnauthorized
		}

		return []byte(jwtKey), nil
	})
	if err != nil {
		return nil, err
	}

	data, ok := token.Claims.(jwt.Claims)
	if !ok || !token.Valid {
		return nil, ahttp.ErrUnauthorized
	}

	datas, err := json.Marshal(data)
	if err != nil {
		return nil, ahttp.ErrUnauthorized
	}

	if err := json.Unmarshal(datas, &claims); err != nil {
		return nil, ahttp.ErrUnauthorized
	}

	return claims, nil
}
package main

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/contracts"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	"log"
	"time"
)

func initDatasource() {
	var err error
	var dbWriter *sqlx.DB
	var dbReader *sqlx.DB

	dsWriter, dsReader := parseDs()

	if dbWriter, err = sqlx.Connect("mysql", dsWriter); err == nil {
		dbWriter.SetConnMaxLifetime(time.Duration(1) * time.Second)
		dbWriter.SetMaxOpenConns(10)
		dbWriter.SetMaxIdleConns(10)

		log.Printf("Initalizing Writer DB: Pass")
	} else {
		log.Panicf("error while connecting to writer db: %s", err)
	}

	if dbReader, err = sqlx.Connect("mysql", dsReader); err == nil {
		dbReader.SetConnMaxLifetime(time.Duration(1) * time.Second)
		dbReader.SetMaxOpenConns(10)
		dbReader.SetMaxIdleConns(10)

		log.Printf("Initalizing Reader DB: Pass")
	} else {
		log.Panicf("error while connecting to reader db: %s", err)
	}

	ds := &contracts.Datasources{
		WriterDB: dbWriter,
		ReaderDB: dbReader,
	}

	app.Datasources = ds

	return
}

func parseDs() (dsWriter, dsReader string) {
	hostWriter := viper.GetString(constants.DbHostWriter)
	hostReader := viper.GetString(constants.DbHostReader)
	port := viper.GetString(constants.DbPort)
	user := viper.GetString(constants.DbUser)
	pass := viper.GetString(constants.DbPass)
	name := viper.GetString(constants.DbName)

	dsWriter = fmt.Sprintf("%s:%s@(%s:%s)/%s", user, pass, hostWriter, port, name)
	dsReader = fmt.Sprintf("%s:%s@(%s:%s)/%s", user, pass, hostReader, port, name)

	return
}

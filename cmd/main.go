package main

import (
	"api.wargakita.id/internal/api/constants"
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/handlers"
	"api.wargakita.id/internal/api/middlewares"
	"api.wargakita.id/internal/api/routers"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris/v12"
	"github.com/spf13/viper"
	"os"
)

var app *contracts.App

func main() {
	os.Setenv("TZ", "Asia/Jakarta")

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowedHeaders:   []string{"Access-Control-Allow-Origin", "Accept", "content-type", "X-Requested-With", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "Screen"},
		AllowCredentials: true,
	})

	irisApp := iris.New()
	irisApp.Use(crs)
	irisApp.AllowMethods(iris.MethodOptions)

	app = &contracts.App{
		Iris: irisApp,
	}

	initConfig()
	initDatasource()
	initServices()

	middlewares.Init(app)
	handlers.Init(app)
	routers.Init(app, crs)

	irisApp.Run(iris.Addr(":"+viper.GetString(constants.ServerPort)), iris.WithOptimizations, iris.WithoutBodyConsumptionOnUnmarshal)

}

package main

import (
	"github.com/spf13/viper"
	"log"
	"os"
)

func initConfig() {
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".env/")

	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Load Configuration Failed, err: %s", err)
		os.Exit(0)
	} else {
		log.Println("Configuration Loaded")
	}
}

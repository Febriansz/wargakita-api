package main

import (
	"api.wargakita.id/internal/api/contracts"
	"api.wargakita.id/internal/api/services/akun"
	"api.wargakita.id/internal/api/services/iuran"
	"api.wargakita.id/internal/api/services/laporan"
	"api.wargakita.id/internal/api/services/logging"
	"api.wargakita.id/internal/api/services/rumah"
	"api.wargakita.id/internal/api/services/warga"
	"log"
)

func initServices() {
	app.Services = &contracts.Services{
		Akun:    akun.Init(app),
		Iuran:   iuran.Init(app),
		Laporan: laporan.Init(app),
		Logging: logging.Init(app),
		Rumah:   rumah.Init(app),
		Warga:   warga.Init(app),
	}

	log.Printf("Initializing Services: Pass")
}

#Step 2
FROM golang:alpine

RUN apk add --no-cache tzdata

ENV TZ=Asia/Jakarta

COPY .env/ .env/
COPY wargakita .

EXPOSE 96

ENTRYPOINT ["./wargakita"]
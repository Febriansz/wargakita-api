module api.wargakita.id

go 1.13

require (
	github.com/CloudyKit/jet/v3 v3.0.0 // indirect
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/aws/aws-sdk-go v1.38.59
	github.com/dgraph-io/badger v1.6.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/etcd-io/bbolt v1.3.3 // indirect
	github.com/gavv/httpexpect v2.0.0+incompatible // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gomodule/redigo v1.7.1-0.20190724094224-574c33c3df38 // indirect
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/iris-contrib/blackfriday v2.0.0+incompatible // indirect
	github.com/iris-contrib/middleware/cors v0.0.0-20210110101738-6d0a4d799b5d
	github.com/iris-contrib/pongo2 v0.0.1 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha2
	github.com/ryanuber/columnize v2.1.0+incompatible // indirect
	github.com/spf13/viper v1.7.1
	github.com/yudai/pp v2.0.1+incompatible // indirect
)

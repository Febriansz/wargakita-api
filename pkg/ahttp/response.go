package ahttp

type Response struct {
	Status  string      `json:"status"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

type ErrorResponse struct {
	Status  int    `json:"status"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

type ErrorDebugResponse struct {
	Status  int    `json:"status"`
	Code    string `json:"code"`
	Message string `json:"message"`
	Debug   string `json:"_debug,omitempty"`
}

// Error is an implementation of built-in error type interface
func (e ErrorResponse) Error() string {
	return e.Message
}
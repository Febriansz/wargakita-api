package alog

import (
	"fmt"
	"log"
	"runtime"
)

func Info(msg string) {
	log.Printf(msg)
}

func Error(err error) {
	pc, fn, line, _ := runtime.Caller(1)
	log.Printf("[error] in %s[%s:%d] %v \n", runtime.FuncForPC(pc).Name(), fn, line, err)
}

func GetTracer(err error) string {
	pc, fn, line, _ := runtime.Caller(1)
	tracer := fmt.Sprintf("[error] in %s[%s:%d] %v \n", runtime.FuncForPC(pc).Name(), fn, line, err)

	return tracer
}

# WargaKita API v1.0

Provide API Service for WargaKita User Portal.

# How to run?

- Clone this repository
- Install `go` language on your machine (min version 1.13)
- Run using `go run -v -a -ldflags="-w -s" ./cmd/`